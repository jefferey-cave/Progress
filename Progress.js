	var ProgBar = {
		start:function(qty){
			qty = qty || 1;
			if(this.getVal() >= this.getMax()){
				$('#prog').attr('value',0);
				$('#prog').attr('max',0);
			}
			var $val = parseInt(this.getMax()) + qty;
			$('#prog').attr('max',$val);
			this.update();
		}
		,finish:function(qty){
			qty = qty || 1;
			if(this.getVal() >= this.getMax()){
				$('#prog').attr('value',1);
				$('#prog').attr('max',1);
			}
			var $val = parseInt(this.getVal()) + qty;
			$('#prog').attr('value',$val);
			this.update();
		}
		,getVal: function(){
			return $('#prog').attr('value');
		}
		,getMax: function(){
			return $('#prog').attr('max');
		}
		,getPct: function(){
			return Math.floor(1000*this.getRatio())/10.0;
		}
		,getRatio: function(){
			return 1.0*this.getVal()/this.getPct;
		}
		,update:function(){
			$('#prog').html(
				'{i} of {I} ({%}%)'
					.replace(/{i}/g,this.getVal())
					.replace(/{I}/g,this.getMax())
					.replace(/{%}/g,this.getPct())
			);
		}
		,init:function(){
			var $progbar = this;
			$(document)
				.bind("ajaxSend", function(){
					$progbar.start();
				})
				.bind("ajaxComplete", function(){
					$progbar.finish();
				})
				.bind("ajaxError", function(){
					$progbar.finish();
				})
				;
		}
	};